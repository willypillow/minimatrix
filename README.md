---
title: "2019 Computer Network Final Project cnMessage Report"
---

(TL;DR: Implementation of a subset of the Matrix.org protocol in Scala.)

# User & Operator Guide #

## User ##

The client can be invoked by `./client PROTOCOL HOST PORT`, where `PROTOCOL` can be `http` or `https`.

The client supports IRC-like commands, listed as follows:

- `/register USERNAME PASSWORD`: Register an account with the credentials.
- `/login USERNAME PASSWORD`: Log in with the credentials.
- `/quit`: Quit.
- `/cd ROOM_ID`: Switch to the room.
- `/ls`: List the rooms that you are in.
- `/join ROOM_ID`: Join the room. Note that you need to be invited first.
- `/invite ROOM_ID USER`: Invite `USER` to the room.
- `/create ROOMNAME MEMBERS`: Create a room named `ROOMNAME` consisting of you and `MEMBERS`.
- `/create --direct USER`: Create a direct chat with `USER`.
- `/up FILE_PATH NAME`: Upload file at `FILE_PATH` as `NAME` and send it to the current room.
- `/down MXC_LINK NAME`: Download file at `MXC_LINK` as `NAME`.

Lines not starting with `/` are interpreted as messages and sent to the current room.

## Operator ##

The configuration file `server/src/Config.scala` provides the following options:

- `TransactorThreads`: Number of threads to use for the database connection pool.
- `ListenInterface`: Interface and port to listen on.
- `ContentRepoPath`: Path to store uploaded files.
- `ServerName`: Host name of the server.
- `JdbcString`, `JdbcUser`, `JdbcPswd`: JDBC information to connect to the database.

For the initial run, one needs to generate an *H2* database with the schema described in `schema.sql`. This can be done via the following steps:

- Download the [H2 JAR](https://www.h2database.com/html/main.html).
- Run `java -cp h2.jar org.h2.tools.Shell`.
- Enter the database information from `Config.scala`.
- Paste the statements from `schema.sql`.
- Type `quit`.

# Instructions on How to Run Server & Clients #

## Client ##

Note that one needs to have the Boost development packages installed. In addition, the HTTPS functionality may not work on systems like Windows due to root certificate -related problems.[^cert]

```
cd src/client
make
./client PROTOCOL HOST PORT
```

[^cert]: Trusted root certificates are fetched using `set_defaults_verify_paths`, which some report to be not working under Windows.

## Server ##

Note that one needs to have Java installed. In addition, due to [a bug](https://github.com/phxql/argon2-jvm/issues/69) in the *argon2-jvm* library, which we use for password hashing, older distros of Linux might not be able to run the server without modifications. It does work, however, on the CSIE workstations to the very least.

```
cd src/server
./mill server.run
```

# System & Program Design #

We implemented a subset of the [Matrix](https://matrix.org/) protocol[^1], an open standard. Specifically, the following endpoints are implemented (though not necessarily in full):

- `POST /_matrix/client/r0/login`
- `POST /_matrix/client/r0/register`
- `GET /_matrix/client/r0/sync`
- `PUT /_matrix/client/r0/rooms/{roomId}/send/{eventType}/{txnId}`
- `POST /_matrix/client/r0/createRoom`
- `POST /_matrix/client/r0/rooms/{roomId}/invite`
- `POST /_matrix/client/r0/rooms/{roomId}/join`
- `POST /_matrix/media/r0/upload`
- `GET /_matrix/media/r0/download/{serverName}/{mediaId}`

The following events are implemented (again not necessarily in full):

- `m.room.create`
- `m.room.member`
- `m.room.message` (`m.text`, `m.file`)

[^1]: Specifically, its client-server protocol, c.f. `https://matrix.org/docs/spec/client_server/r0.6.0`.

The designs of the server and client are descibed below.

## Server ##

The server is written in *Scala*, a language with excellent support for functional programming and a strong static type system that runs on the Java virtual machine.

For the HTTP layer, we incorporated *[Finch](https://finagle.github.io/finch/)*, a purely functional combinator library for building HTTP services. The services are then run on top of *[Finagle](https://github.com/twitter/finagle)*, a highly performant and concurrent RPC system based on *[Netty](https://netty.io/)* that is used extensively in production at companies such as Twitter.

In our endpoints, *futures* are extensively used to prevent blocking the event loop of *Netty*.

TLS is not directly implemented in the server application. Instead, a nginx reverse proxy is placed in front of it.

*[Circe](https://circe.github.io/circe/)*, a purely functional JSON library forked from *Argonaut*, is used for processing JSON.

For the database layer, *[doobie](https://tpolecat.github.io/doobie/)* and *[Quill](https://getquill.io/)* are used. The former is a purely functional JDBC[^2] layer, while the latter provides an ORM-like interface and generates the SQL statements at compile time.

*H2* is chosen as the backend database due to its small footprint and ease of install[^3]. In addition, the `Config.ContentRepoPath` directory is used to place the uploaded files.

The *Argon2d* KDF is used to hash the user passwords.

The server is written in a purely functional manner, utilizing type classes such as `IO` and `EitherT` monads from *[Cats](https://typelevel.org/cats/)*[^4] to handle side effects and errors.

[^2]: *Java Database Connectivity*, since Scala runs on the JVM.

[^3]: In case the TAs want to run our programs ;)

[^4]: "...a library which provides abstractions for functional programming in the Scala programming language."

## Client ##

The client is written in C++ and uses *[boost::beast](http://www.boost.org/libs/beast)* for HTTP(S) and *[nlohmann_json](https://github.com/nlohmann/json)* for JSON.

In addition to the main thread that processes user input, another thread is spawned to contact the `sync` endpoint and print received messages. A shared atomic variable is used to pass messages such as `/ls` commands from the main thread to the sync thread.

Message sending, along with file transfers, are done by calling `std::async` in the main thread so that they do not block.

# Miscellaneous #

## Summary of Bonus Features ##

- Access log
- Auto reconnect
- TLS encryption support
- Group chat
- Ability for the client to interact with other Matrix servers
- File transmission to offline users
- Parallel sending of files and messages
