#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <future>
#include <iostream>
#include <optional>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include "nlohmann/json.hpp"
#pragma clang diagnostic pop

using tcp = boost::asio::ip::tcp;
using json = nlohmann::json;
namespace http = boost::beast::http;
namespace net = boost::asio;  // from <boost/asio.hpp>
namespace ssl = net::ssl;

const std::string kRed = "\x1b[31m", kYellow = "\x1b[33m", kRst = "\x1b[0m";

enum class ThreadMsg { EXIT, NONE, LIST_ROOMS, RESYNC };

std::string host, port;
bool use_tls;

// Based on the official Beast examples
http::response<http::string_body> MakeReq(std::string target, json data,
                                          std::string auth_id = "",
                                          http::verb verb = http::verb::post) {
	// Set up an HTTP POST / PUT request message
	http::request<http::string_body> req{verb, target, 11};
	req.set(http::field::host, host);
	req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
	req.set(http::field::content_type, "application/json");
	if (auth_id.size()) req.set("Authorization", "Bearer " + auth_id);
	req.body() = data.dump();
	req.set(http::field::content_length, req.body().size());

	// The io_context is required for all I/O
	boost::asio::io_context ioc;

	boost::system::error_code ec;

	// Declare a container to hold the response
	http::response<http::string_body> res;

	if (use_tls) {
		// The SSL context is required, and holds certificates
		ssl::context ctx(ssl::context::tlsv12_client);

		// This holds the root certificate used for verification
		ctx.set_default_verify_paths();

		// Verify the remote server's certificate
		ctx.set_verify_mode(ssl::verify_peer);

		// These objects perform our I/O
		tcp::resolver resolver{ioc};
		boost::beast::ssl_stream<boost::beast::tcp_stream> socket(ioc, ctx);

		// Set SNI Hostname (many hosts need this to handshake successfully)
		if (!SSL_set_tlsext_host_name(socket.native_handle(), host.c_str())) {
			boost::beast::error_code ecx{static_cast<int>(::ERR_get_error()),
			                             net::error::get_ssl_category()};
			throw boost::beast::system_error{ecx};
		}

		// Look up the domain name
		auto const results = resolver.resolve(host, port);

		// Make the connection on the IP address we get from a lookup
		boost::beast::get_lowest_layer(socket).connect(results);

		// Perform the SSL handshake
		socket.handshake(ssl::stream_base::client);

		// Send the HTTP request to the remote host
		http::write(socket, req);

		// This buffer is used for reading and must be persisted
		boost::beast::flat_buffer buffer;

		// Receive the HTTP response
		http::read(socket, buffer, res);

		// Gracefully close the socket
		socket.shutdown(ec);
	} else {
		// These objects perform our I/O
		tcp::resolver resolver{ioc};
		tcp::socket socket{ioc};

		// Look up the domain name
		auto const results = resolver.resolve(host, port);

		// Make the connection on the IP address we get from a lookup
		boost::asio::connect(socket, results.begin(), results.end());

		// Send the HTTP request to the remote host
		http::write(socket, req);

		// This buffer is used for reading and must be persisted
		boost::beast::flat_buffer buffer;

		// Receive the HTTP response
		http::read(socket, buffer, res);

		// Gracefully close the socket
		socket.shutdown(tcp::socket::shutdown_both, ec);
	}

	if (ec == net::error::eof ||
	    ec == ssl::error::stream_errors::stream_truncated) {
		// Rationale:
		// http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
		ec = {};
	}

	// not_connected happens sometimes
	// so don't bother reporting it.
	//
	if (ec && ec != boost::system::errc::not_connected)
		throw boost::system::system_error{ec};

	return res;
}

// Based on the official Beast examples
http::message<false, http::file_body> MakeGetFile(std::string target,
                                                  std::string filename,
                                                  std::string auth_id = "") {
	// Set up an HTTP POST / PUT request message
	http::request<http::string_body> req{http::verb::get, target, 11};
	req.set(http::field::host, host);
	req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
	req.set(http::field::content_type, "application/json");
	if (auth_id.size()) req.set("Authorization", "Bearer " + auth_id);

	// The io_context is required for all I/O
	boost::asio::io_context ioc;

	boost::system::error_code ec;

	// Declare a container to hold the response
	http::response_parser<http::file_body> res;
	res.body_limit((std::numeric_limits<std::uint64_t>::max)());
	boost::system::error_code ec0;
	res.get().body().open(filename.c_str(), boost::beast::file_mode::write, ec0);

	if (use_tls) {
		// The SSL context is required, and holds certificates
		ssl::context ctx(ssl::context::tlsv12_client);

		// This holds the root certificate used for verification
		ctx.set_default_verify_paths();

		// Verify the remote server's certificate
		ctx.set_verify_mode(ssl::verify_peer);

		// These objects perform our I/O
		tcp::resolver resolver{ioc};
		boost::beast::ssl_stream<boost::beast::tcp_stream> socket(ioc, ctx);

		// Set SNI Hostname (many hosts need this to handshake successfully)
		if (!SSL_set_tlsext_host_name(socket.native_handle(), host.c_str())) {
			boost::beast::error_code ecx{static_cast<int>(::ERR_get_error()),
			                             net::error::get_ssl_category()};
			throw boost::beast::system_error{ecx};
		}

		// Look up the domain name
		auto const results = resolver.resolve(host, port);

		// Make the connection on the IP address we get from a lookup
		boost::beast::get_lowest_layer(socket).connect(results);

		// Perform the SSL handshake
		socket.handshake(ssl::stream_base::client);

		// Send the HTTP request to the remote host
		http::write(socket, req);

		// This buffer is used for reading and must be persisted
		boost::beast::flat_buffer buffer;

		// Receive the HTTP response
		http::read(socket, buffer, res);

		// Gracefully close the socket
		socket.shutdown(ec);
	} else {
		// These objects perform our I/O
		tcp::resolver resolver{ioc};
		tcp::socket socket{ioc};

		// Look up the domain name
		auto const results = resolver.resolve(host, port);

		// Make the connection on the IP address we get from a lookup
		boost::asio::connect(socket, results.begin(), results.end());

		// Send the HTTP request to the remote host
		http::write(socket, req);

		// This buffer is used for reading and must be persisted
		boost::beast::flat_buffer buffer;

		// Receive the HTTP response
		http::read(socket, buffer, res);

		// Gracefully close the socket
		socket.shutdown(tcp::socket::shutdown_both, ec);
	}

	res.get().body().close();

	if (ec == net::error::eof ||
	    ec == ssl::error::stream_errors::stream_truncated) {
		// Rationale:
		// http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
		ec = {};
	}

	// not_connected happens sometimes
	// so don't bother reporting it.
	//
	if (ec && ec != boost::system::errc::not_connected)
		throw boost::system::system_error{ec};

	return std::move(res.get());
}

// Based on the official Beast examples
http::response<http::string_body> MakePostFile(std::string target,
                                               std::string file_path,
                                               std::string auth_id = "") {
	// Set up an HTTP POST request message
	http::request<http::file_body> req{http::verb::post, target, 11};
	req.set(http::field::host, host);
	req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
	if (auth_id.size()) req.set("Authorization", "Bearer " + auth_id);
	boost::system::error_code ec0;
	req.body().open(file_path.c_str(), boost::beast::file_mode::scan, ec0);
	req.set(http::field::content_length, req.body().size());

	// The io_context is required for all I/O
	boost::asio::io_context ioc;

	boost::system::error_code ec;

	// Declare a container to hold the response
	http::response<http::string_body> res;

	if (use_tls) {
		// The SSL context is required, and holds certificates
		ssl::context ctx(ssl::context::tlsv12_client);

		// This holds the root certificate used for verification
		ctx.set_default_verify_paths();

		// Verify the remote server's certificate
		ctx.set_verify_mode(ssl::verify_peer);

		// These objects perform our I/O
		tcp::resolver resolver{ioc};
		boost::beast::ssl_stream<boost::beast::tcp_stream> socket(ioc, ctx);

		// Set SNI Hostname (many hosts need this to handshake successfully)
		if (!SSL_set_tlsext_host_name(socket.native_handle(), host.c_str())) {
			boost::beast::error_code ecx{static_cast<int>(::ERR_get_error()),
			                             net::error::get_ssl_category()};
			throw boost::beast::system_error{ecx};
		}

		// Look up the domain name
		auto const results = resolver.resolve(host, port);

		// Make the connection on the IP address we get from a lookup
		boost::beast::get_lowest_layer(socket).connect(results);

		// Perform the SSL handshake
		socket.handshake(ssl::stream_base::client);

		// Send the HTTP request to the remote host
		http::write(socket, req);

		// This buffer is used for reading and must be persisted
		boost::beast::flat_buffer buffer;

		// Receive the HTTP response
		http::read(socket, buffer, res);

		// Gracefully close the socket
		socket.shutdown(ec);
	} else {
		// These objects perform our I/O
		tcp::resolver resolver{ioc};
		tcp::socket socket{ioc};

		// Look up the domain name
		auto const results = resolver.resolve(host, port);

		// Make the connection on the IP address we get from a lookup
		boost::asio::connect(socket, results.begin(), results.end());

		// Send the HTTP request to the remote host
		http::write(socket, req);

		// This buffer is used for reading and must be persisted
		boost::beast::flat_buffer buffer;

		// Receive the HTTP response
		http::read(socket, buffer, res);

		// Gracefully close the socket
		socket.shutdown(tcp::socket::shutdown_both, ec);
	}

	if (ec == net::error::eof ||
	    ec == ssl::error::stream_errors::stream_truncated) {
		// Rationale:
		// http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
		ec = {};
	}

	// not_connected happens sometimes
	// so don't bother reporting it.
	//
	if (ec && ec != boost::system::errc::not_connected)
		throw boost::system::system_error{ec};

	return res;
}

std::optional<std::string> LoginReq(std::string username,
                                    std::string password) {
	json j;
	j["type"] = "m.login.password";
	j["identifier"] = {{"type", "m.id.user"}, {"user", username}};
	j["password"] = password;
	auto resp = MakeReq("/_matrix/client/r0/login", j);
	if (resp.result() == http::status::ok) {
		auto rj = json::parse(resp.body());
		return rj["access_token"];
	} else {
		return std::nullopt;
	}
}

std::optional<std::string> RegisterReq(std::string username,
                                       std::string password) {
	json j;
	j["username"] = username;
	j["password"] = password;
	auto resp = MakeReq("/_matrix/client/r0/register", j);
	if (resp.result() == http::status::ok) {
		auto rj = json::parse(resp.body());
		return rj["access_token"];
	} else {
		return std::nullopt;
	}
}

bool JoinReq(std::string auth_id, std::string room_id) {
	json j;
	j["room_id"] = room_id;
	auto resp =
	    MakeReq("/_matrix/client/r0/rooms/" + room_id + "/join", j, auth_id);
	return (resp.result() == http::status::ok);
}

bool InviteReq(std::string auth_id, std::string room_id, std::string user_id) {
	json j;
	j["user_id"] = user_id;
	auto resp =
	    MakeReq("/_matrix/client/r0/rooms/" + room_id + "/invite", j, auth_id);
	return (resp.result() == http::status::ok);
}

std::optional<std::string> CreateRoomReq(std::string auth_id,
                                         std::string room_name,
                                         std::vector<std::string> invites) {
	json j;
	j["name"] =
	    (room_name == "--direct" && invites.size()) ? invites[0] : room_name;
	j["invite"] = invites;
	j["is_direct"] = room_name == "--direct";
	auto resp = MakeReq("/_matrix/client/r0/createRoom", j, auth_id);
	if (resp.result() == http::status::ok) {
		auto rj = json::parse(resp.body());
		return rj["room_id"];
	} else {
		return std::nullopt;
	}
}

bool SendMsg(std::string auth_id, std::string room_id, std::string msg) {
	json j;
	j["msgtype"] = "m.text";
	j["body"] = msg;
	std::string path =
	    "/_matrix/client/r0/rooms/" + room_id + "/send/m.room.message/0x0x0x0";
	auto resp = MakeReq(path, j, auth_id, http::verb::put);
	return (resp.result() == http::status::ok);
}

bool SendFileMsg(std::string auth_id, std::string room_id, std::string uri,
                 std::string filename) {
	json j;
	j["msgtype"] = "m.file";
	j["body"] = filename;
	j["filename"] = filename;
	j["url"] = uri;
	std::string path =
	    "/_matrix/client/r0/rooms/" + room_id + "/send/m.room.message/0x0x0x0";
	auto resp = MakeReq(path, j, auth_id, http::verb::put);
	return (resp.result() == http::status::ok);
}

std::optional<std::string> UploadFile(std::string auth_id,
                                      std::string file_path,
                                      std::string filename) {
	{
		std::ifstream f(file_path.c_str());
		if (!f.good()) return std::nullopt;
	}
	std::string path = "/_matrix/media/r0/upload/?filename=" + filename;
	auto resp = MakePostFile(path, file_path, auth_id);
	if (resp.result() == http::status::ok) {
		auto rj = json::parse(resp.body());
		return rj["content_uri"];
	} else {
		return std::nullopt;
	}
}

bool DownloadFile(std::string auth_id, std::string file_path,
                  std::string filename) {
	std::string path = "/_matrix/media/r0/download/" + file_path;
	auto resp = MakeGetFile(path, filename, auth_id);
	return (resp.result() == http::status::ok);
}

void SyncThread(std::optional<std::string> auth_id,
                std::optional<std::string> username,
                std::atomic<ThreadMsg>* msg) {
	if (!auth_id) return;
	std::unordered_map<std::string, std::string> room2name, room_user;
	std::unordered_map<std::string, bool> is_direct;
	std::unordered_set<std::string> vis_evts, invited;
	std::string a_id = auth_id.value(), user = username.value();
	std::optional<std::string> since;
	while (*msg != ThreadMsg::EXIT) {
		try {
			{
				ThreadMsg tmp = ThreadMsg::LIST_ROOMS;
				if (msg->compare_exchange_strong(tmp, ThreadMsg::NONE)) {
					std::cout << kRed << "[System] " << kRst << "Rooms you are in:\n";
					for (auto& [k, v] : room2name) {
						std::cout << kRed << "[System] " << kRst << k << ": " << v << "\n";
					}
					std::cout << std::flush;
				}
			}
			{
				ThreadMsg tmp = ThreadMsg::RESYNC;
				if (msg->compare_exchange_strong(tmp, ThreadMsg::NONE)) {
					since = std::nullopt;
				}
			}
			std::string path = "/_matrix/client/r0/sync" +
			                   (since ? ("?since=" + since.value()) : "");
			json j;
			auto resp = MakeReq(path, j, a_id, http::verb::get);
			if (resp.result() != http::status::ok) {
				std::cout << kRed << "[System] " << kRst << "Sync failed." << std::endl;
			} else {
				std::vector<
				    std::tuple<int64_t, std::string, std::string, json, std::string>>
				    evts;
				auto rj = json::parse(resp.body());
				since = rj["next_batch"];
				for (auto& [k, v] : rj["rooms"]["join"].items()) {
					for (auto e : v["timeline"]["events"]) {
						if (vis_evts.find(e["event_id"]) == vis_evts.end()) {
							evts.emplace_back(e["origin_server_ts"], k, e["type"],
							                  e["content"], e["sender"]);
						}
					}
				}
				for (auto& [k, v] : rj["rooms"]["invite"].items()) {
					if (invited.find(k) == invited.end()) {
						invited.insert(k);
						std::string name = k;
						for (auto e : v["inviteState"]["events"]) {
							if (e["type"] == "m.room.name") name = e["content"]["name"];
						}
						std::cout << kRed << "[System] " << kRst
						          << "You've been invited to " << name << " (" << k << ")"
						          << std::endl;
					}
				}
				std::sort(evts.begin(), evts.end());
				for (auto& t : evts) {
					if (std::get<2>(t) == "m.room.message") {
						if (std::get<3>(t)["msgtype"] == "m.text") {
							std::cout << kYellow << "[" << room2name[std::get<1>(t)] << "] "
							          << kRst << std::get<4>(t) << ": "
							          << std::get<3>(t)["body"].get<std::string>()
							          << std::endl;
						} else if (std::get<3>(t)["msgtype"] == "m.file") {
							std::cout << kYellow << "[" << room2name[std::get<1>(t)] << "] "
							          << kRst << std::get<4>(t) << " sent a file: "
							          << std::get<3>(t)["body"].get<std::string>() << " ("
							          << std::get<3>(t)["url"] << ")" << std::endl;
						}
					} else if (std::get<2>(t) == "m.room.name") {
						if (!is_direct[std::get<1>(t)]) {
							room2name[std::get<1>(t)] = std::get<3>(t)["name"];
						}
					} else if (std::get<2>(t) == "m.room.member" &&
					           std::get<3>(t)["membership"].get<std::string>() ==
					               "join") {
						std::cout << kYellow << "[" << room2name[std::get<1>(t)] << "] "
						          << kRst << std::get<4>(t) << " joined the room."
						          << std::endl;
					} else if (std::get<2>(t) == "m.room.member" &&
					           std::get<3>(t)["membership"].get<std::string>() ==
					               "invite") {
						if (std::get<4>(t) != user) {
							if (room_user[std::get<1>(t)].size())
								room_user[std::get<1>(t)] += ",";
							room_user[std::get<1>(t)] += std::get<4>(t);
						}
						if (std::get<3>(t)["is_direct"].get<bool>()) {
							is_direct[std::get<1>(t)] = true;
							room2name[std::get<1>(t)] = room_user[std::get<1>(t)];
						}
					} else if (std::get<2>(t) == "m.room.create") {
						room2name[std::get<1>(t)] = std::get<1>(t);
						room_user[std::get<1>(t)] =
						    std::get<4>(t) == user ? "" : std::get<4>(t);
					}
				}
			}
		} catch (std::exception const& e) {
			std::cout << kRed << "[System] " << kRst << "Error: " << e.what()
			          << std::endl;
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

int main(int argc, char** argv) {
	try {
		if (argc != 4) {
			std::cerr << "Usage: " << argv[0] << " <proto> <host> <port>\n";
			return EXIT_FAILURE;
		}
		use_tls = strcmp(argv[1], "https") == 0;
		host = argv[2];
		port = argv[3];

		std::optional<std::string> auth_id, username;
		std::atomic<ThreadMsg> msg = ThreadMsg::EXIT;
		std::string room_id;
		std::thread sync_thread(SyncThread, auth_id, username, &msg);
		sync_thread.detach();
		bool quit = false;

		while (!quit) {
			std::string cmd;
			std::getline(std::cin, cmd);
			if (cmd.empty()) continue;
			if (cmd[0] == '/') {
				std::stringstream ss(cmd);
				std::string op;
				ss >> op;
				if (op == "/login") {
					msg = ThreadMsg::EXIT;
					std::string user, pass;
					ss >> user >> pass;
					auth_id = LoginReq(user, pass);
					if (!auth_id) {
						std::cout << kRed << "[System] " << kRst << "Incorrect credentials."
						          << std::endl;
					} else {
						msg = ThreadMsg::NONE;
						username = user;
						sync_thread = std::thread(SyncThread, auth_id, username, &msg);
						sync_thread.detach();
					}
				} else if (op == "/register") {
					msg = ThreadMsg::EXIT;
					std::string user, pass;
					ss >> user >> pass;
					auth_id = RegisterReq(user, pass);
					if (!auth_id) {
						std::cout << kRed << "[System] " << kRst << "Registration failed."
						          << std::endl;
					} else {
						msg = ThreadMsg::NONE;
						username = user;
						sync_thread = std::thread(SyncThread, auth_id, username, &msg);
						sync_thread.detach();
					}
				} else if (op == "/quit") {
					quit = true;
				} else if (!auth_id) {
					std::cout << kRed << "[System] " << kRst
					          << "Not logged in. Please /login or /register first."
					          << std::endl;
				} else {
					if (op == "/cd") {
						ss >> room_id;
					} else if (op == "/ls") {
						msg = ThreadMsg::LIST_ROOMS;
					} else {
						std::async([=, &msg]() {
							std::stringstream ss_copy(cmd);
							ss_copy.ignore(INT_MAX, ' ');
							if (op == "/join") {
								std::string new_id;
								ss_copy >> new_id;
								if (JoinReq(auth_id.value(), new_id)) {
									std::cout << kRed << "[System] " << kRst << "Joined "
									          << new_id << "." << std::endl;
								} else {
									std::cout << kRed << "[System] " << kRst << "Failed to join "
									          << new_id << "." << std::endl;
								}
								msg = ThreadMsg::RESYNC;
							} else if (op == "/invite") {
								std::string r_id, u_id;
								ss_copy >> r_id >> u_id;
								if (InviteReq(auth_id.value(), r_id, u_id)) {
									std::cout << kRed << "[System] "
									          << "Invited " << u_id << " to " << r_id << "."
									          << std::endl;
								} else {
									std::cout << kRed << "[System] " << kRst
									          << "Failed to invite " << u_id << " to " << r_id
									          << "." << std::endl;
								}
							} else if (op == "/create") {
								std::string name, tmp;
								std::vector<std::string> invites;
								ss_copy >> name;
								while (ss_copy >> tmp) invites.push_back(std::move(tmp));
								auto id = CreateRoomReq(auth_id.value(), name, invites);
								if (id) {
									std::cout << kRed << "[System] " << kRst << "Created "
									          << id.value() << "." << std::endl;
								} else {
									std::cout << kRed << "[System] "
									          << "Creating " << id.value() << " failed."
									          << std::endl;
								}
							} else if (op == "/up") {
								std::string file_path, filename;
								ss_copy >> file_path >> filename;
								std::cout << kRed << "[System] " << kRst << "Uploading "
								          << file_path << "." << std::endl;
								auto uri = UploadFile(auth_id.value(), file_path, filename);
								if (uri) {
									std::cout << kRed << "[System] " << kRst << "Uploaded "
									          << file_path << "." << std::endl;
									if (!SendFileMsg(auth_id.value(), room_id, uri.value(),
									                 filename)) {
										std::cout << kRed << "[System] " << kRst
										          << "Failed to send message." << std::endl;
									}
								} else {
									std::cout << kRed << "[System] " << kRst
									          << "Failed to upload " << file_path << "."
									          << std::endl;
								}
							} else if (op == "/down") {
								std::string id, filename;
								ss_copy >> id >> filename;
								id = id.substr(6);  // Strip mxc://
								if (DownloadFile(auth_id.value(), id, filename)) {
									std::cout << kRed << "[System] " << kRst << "Downloaded "
									          << filename << "." << std::endl;
								} else {
									std::cout << kRed << "[System] " << kRst
									          << "Failed to download " << filename << "."
									          << std::endl;
								}
							} else {
								std::cout << kRed << "[System] " << kRst << "Unknown command."
								          << std::endl;
							}
						});
					}
				}
			} else {
				if (!auth_id) {
					std::cout << kRed << "[System] " << kRst
					          << "Not logged in. Please /login or /register first."
					          << std::endl;
				} else {
					std::async([=]() {
						if (!SendMsg(auth_id.value(), room_id, cmd)) {
							std::cout << kRed << "[System] " << kRst
							          << "Failed to send message." << std::endl;
						}
					});
				}
			}
		}

	} catch (std::exception const& e) {
		std::cout << kRed << "[System] " << kRst << "Error: " << e.what()
		          << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
