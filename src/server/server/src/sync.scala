package pw.nerde.minimatrix

import io.finch._, io.finch.syntax._, io.finch.circe._
import com.twitter.util.FuturePool
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._,
io.circe.syntax._, io.circe.parser.parse
import cats.effect._, cats.implicits._, cats.data.EitherT
import doobie._, doobie.implicits._
import scala.util.Random
import schema.{Event, RoomMembership}

@ConfiguredJsonCodec
case class SyncResp(next_batch: String, rooms: RoomsInfo)

@ConfiguredJsonCodec
case class RoomsInfo(
    join: Map[String, JoinedRoom],
    invite: Map[String, InvitedRoom]
)

@ConfiguredJsonCodec
case class JoinedRoom(
    // state: RoomState,
    timeline: Timeline
)

@ConfiguredJsonCodec
case class InvitedRoom(inviteState: InviteState)

@ConfiguredJsonCodec
case class InviteState(events: Seq[StrippedState])

@ConfiguredJsonCodec
case class StrippedState(
    content: Json,
    stateKey: String,
    `type`: String,
    sender: String
)

// TODO-IMPROVE: Implement `limited` sync
/*
case class RoomState(events: Seq[StateEvent])

case class StateEvent(
  content: Json,
  `type`: String,
  eventId: String,
  sender: String,
  originServerTs: Long,
  stateKey: String)
 */

@ConfiguredJsonCodec
case class Timeline(events: Seq[RoomEvent])

@ConfiguredJsonCodec
case class RoomEvent(
    content: Json,
    `type`: String,
    eventId: String,
    sender: String,
    originServerTs: Long
)

class SyncEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  type Err = Output[Nothing]
  val ep =
    get(
      "_matrix" ::
        "client" ::
        "r0" ::
        "sync" ::
        paramOption("since") ::
        paramOption[Int]("timeout") ::
        header("Authorization")
    ) { (sinceOpt: Option[String], timeoutOpt: Option[Int], authId: String) =>
      FuturePool.unboundedPool {
        import Config.quillCtx._
        val since = sinceOpt.map(_.toLong).getOrElse(Long.MinValue)
        lazy val evtId = Random.alphanumeric.take(64).mkString
        val resp =
          for {
            user <- Require.auth(authId)
            joinQ = quote {
              query[Event]
                .filter { x =>
                  x.originServerTs > lift(since) && query[RoomMembership]
                    .filter(
                      y =>
                        y.roomId == x.roomId && y.username == lift(user) && !y.onlyInvite
                    )
                    .nonEmpty
                }
            }
            res <- EitherT.right[Err](run(joinQ).transact(xa))
            join = res
              .groupBy(_.roomId)
              .map {
                case (id, seq) =>
                  val retSeq = seq.map(
                    x =>
                      RoomEvent(
                        parse(x.content).getOrElse("".asJson),
                        x.evtType,
                        x.evtId,
                        x.sender,
                        x.originServerTs
                      )
                  )
                  id -> JoinedRoom(Timeline(retSeq))
              }
              .toMap
            invQ = quote {
              query[Event]
                .filter { x =>
                  (x.evtType == "m.room.name" || x.evtType == "m.room.member") &&
                  query[RoomMembership].filter { y =>
                    y.roomId == x.roomId && y.username == lift(user) && y.onlyInvite
                  }.nonEmpty
                }
            }
            invRes <- EitherT.right[Err](run(invQ).transact(xa))
            invs = invRes
              .groupBy(_.roomId)
              .map {
                case (id, seq) =>
                  val retSeq = seq.map { x =>
                    StrippedState(
                      parse(x.content).getOrElse("".asJson),
                      x.stateKey.getOrElse(""),
                      x.evtType,
                      x.sender
                    )
                  }
                  id -> InvitedRoom(InviteState(retSeq))
              }
              .toMap
            time <- EitherT.rightT[IO, Err](System.currentTimeMillis())
          } yield Ok(SyncResp((time - 1).toString, RoomsInfo(join, invs)))
        resp.fold(identity, identity).unsafeRunSync()
      }
    }
}
