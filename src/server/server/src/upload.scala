package pw.nerde.minimatrix

import cats.effect._, cats.implicits._, cats.data.EitherT
import com.twitter.util.FuturePool
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._
import io.finch._, io.finch.syntax._, io.finch.circe._
import java.io.{BufferedOutputStream, FileOutputStream}
import scala.util.Random

@ConfiguredJsonCodec
case class UploadResp(contentUri: String)

class UploadEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  type Err = Output[Nothing]
  val ep =
    post(
      "_matrix" ::
        "media" ::
        "r0" ::
        "upload" ::
        param("filename") ::
        header("Authorization") ::
        binaryBody
    ) { (filename: String, authId: String, file: Array[Byte]) =>
      FuturePool.unboundedPool {
        println(file.size)
        lazy val fileId = Random.alphanumeric.take(64).mkString
        val resp =
          for {
            user <- Require.auth(authId)
            writeFile <- EitherT.right[Err](IO {
              println(file.size)
              val bos = new BufferedOutputStream(
                new FileOutputStream(Config.ContentRepoPath + "/" + fileId)
              )
              bos.write(file)
              bos.close()
            })
          } yield Ok(UploadResp(s"mxc://${Config.ServerName}/${fileId}"))
        resp.fold(identity, identity).unsafeRunSync()
      }
    }
}
