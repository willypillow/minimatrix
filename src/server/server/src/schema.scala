package pw.nerde.minimatrix.schema

case class User(username: String, passwordHash: String)

case class UserAuth(username: String, accessToken: String)

case class Room(roomId: String, name: String, isDirect: Boolean)

case class RoomMembership(
    roomId: String,
    username: String,
    onlyInvite: Boolean,
    inviteSender: String
)

case class Event(
    roomId: String,
    evtId: String,
    evtType: String,
    content: String,
    originServerTs: Long,
    sender: String,
    stateKey: Option[String]
)

case class RoomCreateEvent(creator: String)

case class NameEvent(name: String)

case class MemberEvent(membership: String, is_direct: Boolean)
