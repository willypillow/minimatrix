package pw.nerde.minimatrix

import cats.effect._, cats.implicits._, cats.data.EitherT
import com.twitter.util.FuturePool
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._,
io.circe.syntax._
import io.finch._, io.finch.syntax._, io.finch.circe._
import scala.util.Random
import schema.{Event, MemberEvent, Room, RoomMembership}

@ConfiguredJsonCodec
case class JoinResp(roomId: String)

class JoinEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  type Err = Output[Nothing]
  import Config.quillCtx._

  val ep =
    post(
      "_matrix" ::
        "client" ::
        "r0" ::
        "rooms" ::
        path[String] ::
        "join" ::
        header("Authorization")
    ) { (roomId: String, authId: String) =>
      FuturePool.unboundedPool {
        lazy val evtId = Random.alphanumeric.take(64).mkString
        val resp =
          for {
            user <- Require.auth(authId)
            _ <- Require.invite(roomId, user)
            time <- EitherT.rightT[IO, Err](System.currentTimeMillis())
            qDirect = quote {
              query[Room]
                .filter(x => x.roomId == lift(roomId) && x.isDirect)
                .nonEmpty
            }
            isDirect <- EitherT.right[Err](run(qDirect).transact(xa))
            evtJson = MemberEvent("join", isDirect).asJson.dropNullValues.noSpaces
            updMem = quote {
              query[RoomMembership]
                .filter(
                  x => x.roomId == lift(roomId) && x.username == lift(user)
                )
                .update(_.onlyInvite -> false)
            }
            updEvt = quote {
              query[Event].insert(
                Event(
                  lift(roomId),
                  lift(evtId),
                  "m.room.member",
                  lift(evtJson),
                  lift(time),
                  lift(user),
                  Some("")
                )
              )
            }
            trans = for {
              _ <- run(updMem)
              _ <- run(updEvt)
            } yield ()
            _ <- EitherT.right[Err](trans.transact(xa))
          } yield Ok(JoinResp(roomId))
        resp.fold(identity, identity).unsafeRunSync()
      }
    }
}
