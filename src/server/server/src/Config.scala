package pw.nerde

import io.circe.generic.extras.{Configuration => CirceConf}

package object minimatrix {
  object Config {
    val TransactorThreads = 16
    val ListenInterface = ":8081"
    val ContentRepoPath = "/tmp/minimatrix-content"
    val ServerName = "localhost"
    val JdbcString = "jdbc:h2:./database;DB_CLOSE_DELAY=-1"
    val JdbcUser = "sa"
    val JdbcPswd = ""

    val quillCtx = new doobie.quill.DoobieContext.H2(io.getquill.Literal)
  }

  implicit val conf: CirceConf =
    CirceConf.default.withSnakeCaseMemberNames.copy(useDefaults = true)
}
