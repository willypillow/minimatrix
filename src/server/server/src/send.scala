package pw.nerde.minimatrix

import cats.effect._, cats.implicits._, cats.data.EitherT
import com.twitter.util.FuturePool
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._,
io.circe.syntax._
import io.finch._, io.finch.syntax._, io.finch.circe._
import scala.util.Random
import schema.Event

@ConfiguredJsonCodec
case class MsgEvent(msgtype: String, body: String, url: Option[String])

class SendMsgEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  type Err = Output[Nothing]

  val ep =
    put(
      "_matrix" ::
        "client" ::
        "r0" ::
        "rooms" ::
        path[String] ::
        "send" ::
        "m.room.message" ::
        path[String] ::
        jsonBody[MsgEvent] ::
        header("Authorization")
    ) { (roomId: String, txnId: String, event: MsgEvent, authId: String) =>
      FuturePool.unboundedPool {
        import Config.quillCtx._
        lazy val evtId = Random.alphanumeric.take(64).mkString
        val resp =
          for {
            user <- Require.auth(authId)
            _ <- Require.member(roomId, user)
            time <- EitherT.rightT[IO, Err](System.currentTimeMillis())
            msgEvt = Event(
              roomId,
              evtId,
              "m.room.message",
              event.asJson.dropNullValues.noSpaces,
              time,
              user,
              None
            )
            insMsg = quote { query[Event].insert(lift(msgEvt)) }
            _ <- EitherT.right[Err](run(insMsg).transact(xa))
          } yield Ok(())
        resp.fold(identity, identity).unsafeRunSync()
      }
    }
}
