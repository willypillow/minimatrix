package pw.nerde.minimatrix

import cats.effect._, cats.implicits._, cats.data.EitherT
import doobie._, doobie.implicits._
import io.finch._
import io.getquill.{idiom => _, _}
import schema.{RoomMembership, UserAuth}

object Require {
  type Err = Output[Nothing]
  import Config.quillCtx._

  def auth(
      authId: String
  )(implicit xa: Transactor[IO]): EitherT[IO, Err, String] = {
    // "Bearer XX"
    // TODO-IMPROVE: Do this in a better way
    val id = authId.dropWhile(_ != ' ').tail
    val userAuth = quote {
      query[UserAuth].filter(_.accessToken == lift(id)).take(1)
    }
    EitherT(run(userAuth).transact(xa).map {
      _.map(_.username).headOption match {
        case None       => Left(Forbidden(new Exception))
        case Some(user) => Right(user)
      }
    })
  }

  def invite(roomId: String, user: String)(
      implicit xa: Transactor[IO]
  ): EitherT[IO, Err, String] =
    checkMembership(roomId, user, true)

  def member(roomId: String, user: String)(
      implicit xa: Transactor[IO]
  ): EitherT[IO, Err, String] =
    checkMembership(roomId, user, false)

  def checkMembership(roomId: String, user: String, onlyInvite: Boolean)(
      implicit xa: Transactor[IO]
  ): EitherT[IO, Err, String] = {
    val q = quote {
      query[RoomMembership]
        .filter(
          x =>
            x.roomId == lift(roomId) && x.username == lift(user) && (lift(
              onlyInvite
            ) || !x.onlyInvite)
        )
        .nonEmpty
    }
    EitherT(run(q).transact(xa).map { b =>
      if (b) Right(roomId)
      else Left(Forbidden(new Exception))
    })
  }
}
