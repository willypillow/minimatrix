package pw.nerde.minimatrix

import cats.effect._, cats.implicits._
import com.twitter.conversions.storage._
import com.twitter.io.{Buf, Reader}, com.twitter.concurrent.AsyncStream
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._
import io.finch._, io.finch.syntax._, io.finch.circe._
import java.io.File

class DownloadEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  val ep =
    get(
      "_matrix" ::
        "media" ::
        "r0" ::
        "download" ::
        path[String] ::
        path[String]
    ) { (serverName: String, mediaId: String) =>
      // TODO: Better error handling
      require(serverName == Config.ServerName)
      require(mediaId.matches("^[a-zA-Z0-9]*$"))
      val path = Config.ContentRepoPath + "/" + mediaId
      val reader = Reader.fromFile(new File(path))
      Ok(
        AsyncStream.fromReader(reader, chunkSize = 512.kilobytes.inBytes.toInt)
      )
    }
}
