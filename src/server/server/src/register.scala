package pw.nerde.minimatrix

import cats.effect._, cats.implicits._
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._
import io.finch._, io.finch.syntax._, io.finch.circe._
import scala.util.Random
import schema.{User, UserAuth}
import util.Argon2

@ConfiguredJsonCodec
case class RegisterReq(username: String, password: String)

@ConfiguredJsonCodec
case class RegisterResp(userId: String, accessToken: String, deviceId: String)

class RegisterEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  val ep =
    post(
      "_matrix" ::
        "client" ::
        "r0" ::
        "register" ::
        jsonBody[RegisterReq]
    ) { req: RegisterReq =>
      import Config.quillCtx._
      val user = quote {
        query[User].filter(_.username == lift(req).username).isEmpty
      }
      val create = for {
        avail <- run(user).transact(xa)
        newAcc <- if (!avail)
          IO.pure(Forbidden(new Exception))
        else {
          val hash = Argon2.hash(req.password)
          val ins = quote {
            query[User].insert(User(lift(req).username, lift(hash)))
          }
          val token = Random.alphanumeric.take(64).mkString
          val ins2 = quote {
            query[UserAuth]
              .insert(UserAuth(lift(req).username, lift(token)))
          }
          val trans =
            for {
              _ <- run(ins)
              _ <- run(ins2)
            } yield ()
          trans
            .transact(xa)
            .map(_ => Ok(RegisterResp(req.username, token, "0" * 32)))
        }
      } yield newAcc
      create.unsafeRunSync()
    }
}
