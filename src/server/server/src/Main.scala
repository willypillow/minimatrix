package pw.nerde.minimatrix

import cats.effect._, cats.implicits._
import com.twitter.conversions.storage._
import com.twitter.finagle.Http
import com.twitter.finagle.http.filter.{CommonLogFormatter, LoggingFilter}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.logging.Logger
import com.twitter.util.Await
import doobie._, doobie.implicits._, doobie.h2._
import io.circe.generic.auto._
import io.circe.generic.extras.{Configuration => CirceConf}
import io.finch._, io.finch.syntax._, io.finch.circe._

object MyLoggingFilter
    extends LoggingFilter[Request]({
      val log = Logger("access")
      log.setUseParentHandlers(true)
      log
    }, new CommonLogFormatter)

object Main extends IOApp {
  implicit val conf: CirceConf =
    CirceConf.default.withSnakeCaseMemberNames.copy(useDefaults = true)

  val transactor: Resource[IO, Transactor[IO]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[IO](Config.TransactorThreads)
      be <- Blocker[IO]
      xa <- H2Transactor.newH2Transactor[IO](
        Config.JdbcString,
        Config.JdbcUser,
        Config.JdbcPswd,
        ce,
        be
      )
    } yield xa

  def run(args: List[String]): IO[ExitCode] =
    transactor.use { xa =>
      implicit val trans = xa
      val jsonEps =
        (new CreateRoomEndpoint).ep :+:
          (new InviteEndpoint).ep :+:
          (new LoginEndpoint).ep :+:
          (new JoinEndpoint).ep :+:
          (new RegisterEndpoint).ep :+:
          (new SendMsgEndpoint).ep :+:
          (new SyncEndpoint).ep :+:
          (new UploadEndpoint).ep
      val binEps = (new DownloadEndpoint).ep
      val services = Bootstrap
        .serve[Application.Json](jsonEps)
        .serve[Application.OctetStream](binEps)
        .toService

      for {
        _ <- IO(
          Await.ready(
            Http.server
              .withMaxRequestSize(100.megabytes)
              .serve(Config.ListenInterface, MyLoggingFilter.andThen(services))
          )
        )
      } yield ExitCode.Success
    }

}
