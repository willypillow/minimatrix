package pw.nerde.minimatrix

import cats.effect._, cats.implicits._
import com.twitter.util.FuturePool
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._
import io.finch._, io.finch.syntax._, io.finch.circe._
import io.getquill.{idiom => _, _}
import scala.util.Random
import schema.{User, UserAuth}
import util.Argon2

@ConfiguredJsonCodec
case class LoginReq(`type`: String, identifier: UserId, password: String)

@ConfiguredJsonCodec
case class UserId(`type`: String, user: String)

@ConfiguredJsonCodec
case class LoginResp(userId: String, accessToken: String, deviceId: String)

class LoginEndpoint(
    implicit conf: Configuration,
    xa: Transactor[IO]
) {
  val ep =
    post(
      "_matrix" ::
        "client" ::
        "r0" ::
        "login" ::
        jsonBody[LoginReq]
    ) { req: LoginReq =>
      FuturePool.unboundedPool {
        if (req.`type` != "m.login.password")
          BadRequest(new java.lang.UnsupportedOperationException)
        else {
          import Config.quillCtx._
          val user = quote {
            query[User].filter(_.username == lift(req).identifier.user).take(1)
          }
          val verify = run(user).transact(xa).flatMap { l =>
            val lis =
              l.filter(r => Argon2.verify(r.passwordHash, req.password)).map {
                res =>
                  val token = Random.alphanumeric.take(64).mkString
                  val ins = quote {
                    query[UserAuth]
                      .insert(UserAuth(lift(req).identifier.user, lift(token)))
                  }
                  run(ins)
                    .transact(xa)
                    .map(
                      _ => Ok(LoginResp(req.identifier.user, token, "0" * 32))
                    )
              }
            lis.headOption getOrElse IO.pure(Forbidden(new Exception))
          }
          verify.unsafeRunSync()
        }
      }
    }
}
