package pw.nerde.minimatrix.util

import de.mkammerer.argon2.Argon2Factory

object Argon2 {
  val argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2d)

  def hash(in: String): String = argon2.hash(4, 128 * 1024, 8, in.getBytes)

  def verify(hash: String, password: String): Boolean =
    argon2.verify(hash, password.getBytes)
}
