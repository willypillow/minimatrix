package pw.nerde.minimatrix

import cats.effect._, cats.implicits._, cats.data.EitherT
import com.twitter.util.FuturePool
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._,
io.circe.syntax._
import io.finch._, io.finch.syntax._, io.finch.circe._
import io.getquill.{idiom => _, _}
import scala.util.Random
import schema.{
  Event,
  MemberEvent,
  NameEvent,
  Room,
  RoomCreateEvent,
  RoomMembership
}

@ConfiguredJsonCodec
case class CreateRoomReq(name: String, invite: Seq[String], isDirect: Boolean)

@ConfiguredJsonCodec
case class CreateRoomResp(roomId: String)

class CreateRoomEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  type Err = Output[Nothing]
  val ep =
    post(
      "_matrix" ::
        "client" ::
        "r0" ::
        "createRoom" ::
        jsonBody[CreateRoomReq] ::
        header("Authorization")
    ) { (req: CreateRoomReq, authId: String) =>
      FuturePool.unboundedPool {
        import Config.quillCtx._
        lazy val roomId = Random.alphanumeric.take(64).mkString
        lazy val evtId = Random.alphanumeric.take(64).mkString
        lazy val evtId2 = Random.alphanumeric.take(64).mkString
        lazy val newRoom = Room(roomId, req.name, req.isDirect)
        val resp =
          for {
            user <- Require.auth(authId)
            time <- EitherT.rightT[IO, Err](System.currentTimeMillis())
            createState = Event(
              roomId,
              evtId,
              "m.room.create",
              RoomCreateEvent(user).asJson.dropNullValues.noSpaces,
              time,
              user,
              Some("")
            )
            insRoom = quote { query[Room].insert(lift(newRoom)) }
            insEvt = quote { query[Event].insert(lift(createState)) }
            evtJson = MemberEvent("invite", req.isDirect).asJson.dropNullValues.noSpaces
            invIds = req.invite.zip(
              Stream.continually(Random.alphanumeric.take(64).mkString)
            )
            insEvt2 = quote {
              liftQuery(invIds).foreach {
                case (u, e) =>
                  query[Event].insert(
                    Event(
                      lift(roomId),
                      e,
                      "m.room.member",
                      lift(evtJson),
                      lift(time),
                      u,
                      Some("")
                    )
                  )
              }
            }
            insInv = quote {
              liftQuery(req.invite :+ user).foreach { u =>
                query[RoomMembership].insert(
                  RoomMembership(
                    lift(roomId),
                    u,
                    onlyInvite = u != lift(user),
                    lift(user)
                  )
                )
              }
            }
            nameJson = NameEvent(req.name).asJson.dropNullValues.noSpaces
            insName = quote {
              query[Event].insert(
                Event(
                  lift(roomId),
                  lift(evtId2),
                  "m.room.name",
                  lift(nameJson),
                  lift(time),
                  lift(user),
                  Some("")
                )
              )
            }
            trans = for {
              _ <- run(insRoom)
              _ <- run(insEvt)
              _ <- run(insEvt2)
              _ <- run(insInv)
              _ <- run(insName)
            } yield ()
            _ <- EitherT.right[Err](trans.transact(xa))
          } yield Ok(CreateRoomResp(roomId))
        resp.fold(identity, identity).unsafeRunSync()
      }
    }
}
