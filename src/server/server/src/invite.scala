package pw.nerde.minimatrix

import cats.effect._, cats.implicits._, cats.data.EitherT
import com.twitter.util.FuturePool
import doobie._, doobie.implicits._
import io.circe._, io.circe.generic.auto._, io.circe.generic.extras._,
io.circe.syntax._
import io.finch._, io.finch.syntax._, io.finch.circe._
import scala.util.Random
import schema.{Event, MemberEvent, Room, RoomMembership}

@ConfiguredJsonCodec
case class InviteReq(userId: String)

class InviteEndpoint(implicit conf: Configuration, xa: Transactor[IO]) {
  type Err = Output[Nothing]
  val ep: Endpoint[Unit] =
    post(
      "_matrix" ::
        "client" ::
        "r0" ::
        "rooms" ::
        path[String] ::
        "invite" ::
        jsonBody[InviteReq] ::
        header("Authorization")
    ) { (roomId: String, req: InviteReq, authId: String) =>
      FuturePool.unboundedPool {
        import Config.quillCtx._
        lazy val evtId = Random.alphanumeric.take(64).mkString
        val resp =
          for {
            user <- Require.auth(authId)
            time <- EitherT.rightT[IO, Err](System.currentTimeMillis())
            qDirect = quote {
              query[Room]
                .filter(x => x.roomId == lift(roomId) && x.isDirect)
                .nonEmpty
            }
            isDirect <- EitherT.right[Err](run(qDirect).transact(xa))
            evtJson = MemberEvent("invite", isDirect).asJson.dropNullValues.noSpaces
            insInv = quote {
              query[RoomMembership].insert(
                RoomMembership(
                  lift(roomId),
                  lift(req.userId),
                  onlyInvite = true,
                  lift(user)
                )
              )
            }
            updEvt = quote {
              query[Event].insert(
                Event(
                  lift(roomId),
                  lift(evtId),
                  "m.room.member",
                  lift(evtJson),
                  lift(time),
                  lift(req.userId),
                  Some("")
                )
              )
            }
            trans = for {
              _ <- run(insInv)
              _ <- run(updEvt)
            } yield ()
            _ <- EitherT.right[Output[Nothing]](trans.transact(xa))
          } yield Ok(())
        resp.fold(identity, identity).unsafeRunSync()
      }
    }
}
