create table User (
  username varchar(255) primary key,
  passwordHash varchar(255) not null
);

create table UserAuth (
  username varchar(255) not null,
  accessToken varchar(255) not null
);

create table Room (
  roomId varchar(255) primary key,
  name varchar(255) not null,
  isDirect boolean not null
);

create table RoomMembership (
  roomId varchar(255) not null,
  username varchar(255) not null,
  onlyInvite boolean not null,
  inviteSender varchar(255) not null
);

create table Event (
  roomId varchar(255) not null,
  evtId varchar(255) primary key,
  evtType varchar(255) not null,
  content varchar(66000) not null,
  originServerTs bigint not null,
  sender varchar(255) not null,
  stateKey varchar(255)
);
