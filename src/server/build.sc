import mill._, scalalib._, scalafmt._

val scalaVer = "2.12.10"
val finchVer = "0.22.0"
val circeVer = "0.12.2"
val doobieVer = "0.8.8"
val argonJvmVer = "2.6"

object server extends ScalaModule with ScalafmtModule {
  def scalaVersion = scalaVer
  def ivyDeps = Agg(
    ivy"com.github.finagle::finch-core:${finchVer}",
    ivy"com.github.finagle::finch-circe:${finchVer}",
    ivy"io.circe::circe-core:${circeVer}",
    ivy"io.circe::circe-generic:${circeVer}",
    ivy"io.circe::circe-parser:${circeVer}",
    ivy"io.circe::circe-generic-extras:${circeVer}",
    ivy"org.tpolecat::doobie-core:${doobieVer}",
    ivy"org.tpolecat::doobie-h2:${doobieVer}",
    ivy"org.tpolecat::doobie-quill:${doobieVer}",
    ivy"de.mkammerer:argon2-jvm:${argonJvmVer}")
    //ivy"com.lihaoyi:ammonite:2.0.4"
  def scalacOptions = Seq(
    "-deprecation",
    "-Ybackend-parallelism", "16",
    "-Ypartial-unification")
  def scalacPluginIvyDeps =
    super.scalacPluginIvyDeps() ++
    Agg(ivy"org.scalamacros:::paradise:2.1.1") ++
    Agg(ivy"com.olegpy::better-monadic-for:0.3.0")
}
