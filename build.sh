#!/bin/bash

cd src/client
make || exit 1

echo "Client built!"
echo 'Run via the command `cd src/client; ./client PROTO HOST PORT`.'

cd ../..

cd src/server
./mill server.compile

echo "Server built!"
echo 'Run via the command `cd src/server`.' || exit 1
